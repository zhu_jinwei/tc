# Security
Security SIG负责接受和响应openGauss产品安全问题报告，提供社区安全指导，开展安全治理的组织，致力于为openGauss用户提供最安全的产品和开发环境。主要工作职责包括：
制定完善的安全流程：确保社区有一套完善可靠的安全处理流程
响应安全问题：响应上报的安全问题，跟踪安全问题的处理进展，并遵循安全问题批露策略对安全问题在社区内进行披露和公告
漏洞修复：确保及时修复openGauss社区已知漏洞
安全能力演进：推动产品安全能力持续演进，提供完善的安全机制
参与代码审核：通过代码审核帮助团队提前发现代码中的漏洞

# 组织会议

- 公开的会议时间：北京时间，每周三下午，17点00~18点00

# 成员
### Maintainer列表

- zhujinwei[@zhu_jinwei](https://gitee.com/zhu_jinwei)，*zhujinwei@huawei.com*


### Committer列表

- zhangyaozhong[@buter](https://gitee.com/buter), *zhangyaozhong1@huawei.com*

- lijianfeng[@bufee](https://gitee.com/bufee), *bigtimer@qq.com*

- guoliang[@blueloveki](https://gitee.com/blueloveki), *liang4358@163.com*

# 联系方式
- [邮件列表](security@opengauss.org)
- 邮件列表
- IRC公开会议

# 仓库清单

仓库地址：

- https://gitee.com/opengauss/openGauss-server

- https://gitee.com/opengauss/openGauss-third_party

- https://gitee.com/opengauss/security
