# StorageEngine

开发：
- https://gitee.com/opengauss

CI构建：
- https://jenkins.opengauss.org

门户：
- https://opengauss.org

邮箱：
- https://mailweb.opengauss.org

# 组织会议

- 公开的会议时间：北京时间，每周四下午，16点30~17点30

# 成员
### Maintainer列表

- stanley[@stanleyren](https://gitee.com/stanleyren)，*stanleyren_china@outlook.com*


### Committer列表

- zhangjinyu[@18510919607](https://gitee.com/18510919607)，*Jinyu_gaussdb@163.com*


# 联系方式
- [开发邮箱](community@opengauss.org)
- 邮件列表
- IRC公开会议
- 视频会议


# 仓库清单


仓库地址：

- https://gitee.com/opengauss/openGauss-server

- https://gitee.com/opengauss/openGauss-third_party

